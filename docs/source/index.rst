.. Project-Firmware documentation master file, created by
   sphinx-quickstart on Thu Mar 29 11:41:09 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Project-Firmware documentation
==========================================

This page was created to help developers to maintain and change the firmware of 
the Project tools::

Guide
^^^^^

.. toctree::
   :maxdepth: 2

   help
   general_index
   license
