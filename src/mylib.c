#include "mylib.h"

void inc(int *i) {
    *i = *i + 1;
}

void dec(int *i) {
    *i = *i - 1;
}
