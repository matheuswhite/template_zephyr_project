/*! @file
 *  @brief Basic code.
 *         This is a demonstration of how to use doxygen.
 *
 *  Second explanation.
 */

#include <zephyr.h>
#include "version.h"
#define SYS_LOG_DOMAIN "MAIN"
#include <logging/sys_log.h>

#include "mylib.h"

void main(void)
{
    SYS_LOG_WRN("Firmware version: VSX02-v%d.%d.%d",
                VERSION_MAJOR, VERSION_MINOR, VERSION_BUILD);
    int count = 0, count2 = 1000;
    while (1) {
        inc(&count);
        dec(&count2);
        k_sleep(K_MSEC(2));
        printk("ping %d, %d\n", count, count2);
        __ASSERT(count != count2, "*** End of execution!\n");
    }
}
