#ifndef MYLIB_H
#define MYLIB_H

/*! @brief This is an increment function.
 *  This function increments a number.
 *  @param i integer to be incremented
 */
void inc(int *i);

/*! @brief This is a decrement function.
 *  This function decrements a number.
 *  @param i integer to be decremented
 */
void dec(int *i);

#endif // MYLIB_H
